defmodule ChannelApi.SessionController do
  use ChannelApi.Web, :controller

  def unauthenticated_api(conn, _params) do
    json(conn, "No")
  end
end
