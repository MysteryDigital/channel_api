defmodule ChannelApi.Api.V1.ChannelController do
  use ChannelApi.Web, :controller

  alias ChannelApi.SessionController

  plug Guardian.Plug.EnsureAuthenticated, on_failure: { SessionController, :unauthenticated_api }

  def index(conn, _params) do
    json(conn, %{ current_user: Guardian.Plug.current_token(conn) })
  end

end