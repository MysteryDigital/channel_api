# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :channel_api,
  ecto_repos: [ChannelApi.Repo]

# Configures the endpoint
config :channel_api, ChannelApi.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "bU7QtRx9puJN94wvjo4g3s329MXQlMBkq49j11gudM4ilLLl+HDhn3W8wp8y0CLU",
  render_errors: [view: ChannelApi.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ChannelApi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# Auth
config :guardian, Guardian,
  allowed_algos: ["HS256"], # optional
  verify_module: Guardian.JWT,  # optional
  issuer: "ChannelAPI",
  ttl: { 30, :days },
  allowed_drift: 2000,
  verify_issuer: true, # optional
  secret_key: "82ec5479-b8d9-4be6-856a-3b8652b8d9b6",
  serializer: ChannelApi.GuardianSerializer
